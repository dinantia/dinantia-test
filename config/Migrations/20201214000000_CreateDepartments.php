<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class CreateDepartments extends AbstractMigration
{
    /**
     * @return void
     */
    public function up()
    {
        $this->table('departments')
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addIndex(['name'])
            ->create();

        $this->execute("INSERT INTO departments (name) VALUES
            ('IT'),
            ('HR'),
            ('I+D'),
            ('Accounting');
        ");
    }

    /**
     * @return void
     */
    public function down()
    {
        $this->table('departments')
            ->drop()
            ->save();
    }

}
