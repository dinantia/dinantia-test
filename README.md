# Setup

## Backend
Prerequisites:
1. php (7.2+) with the following extensions: `mbstring, intl, simplexml, pdo, sqlite3`
1. composer ([https://getcomposer.org/download/]())

> Note: the following commands should be run from within the repository's root directory (where this file lies)

Install dependencies:
1. `php composer.phar install`

Initialize the database:
1. `bin/cake migrations migrate`

Start the dev server:
1. `bin/cake server -p 8765`
1. Check that it is working at [http://localhost:8765/]()

## Frontend
First, install Ionic and its dependencies: [https://ionicframework.com/docs/intro/cli]()  
Yo will start from a blank app, generated with `ionic start ionicApp blank`.
You will have to install the project dependencies. To do so, `cd` into the `ionicApp` directory and run `npm i`.

To develop, open the app in the browser with `ionic serve`.

# Coding

1. Switch to a develop branch (`git checkout -b develop`)
1. Complete as many exercises as you want (you don't need to complete all of them). Try to save each exercise in a different commit.
1. Once you're done, generate the patches of your changes (`git format-patch master`) and send the `*.patch` files back to us, along with any additional comments you'd like to include.

# Exercises
> Layout and styling are totally up to you. You can create as many services, pipes, components, etc. as you find appropriate. 

### Exercise 1
Turn the home page into a list of departments. Add an option to sort by name (asc/desc).

You already have the base api call ready at `/departments/index.json`.

* https://book.cakephp.org/4/en/orm/query-builder.html

### Exercise 2
Create a Department page. It should open when a user taps a department in the previous page.

You should define a new endpoint `/departments/view/:id.json`

* https://book.cakephp.org/4/en/controllers.html#controller-actions

### Exercise 3
1. Create a Users model. Each user should have a name, and exactly one department.
2. Add a form to create a new user in the Department page.
3. Add a list of users to the Department page. Its corresponding api call should have the form `/users/index.json?department_id=:id`.
4. Add a `Delete user` functionality.

* https://book.cakephp.org/migrations/3/en/index.html
* https://book.cakephp.org/4/en/orm/saving-data.html
* https://book.cakephp.org/4/en/orm/deleting-data.html

### Exercise 4
Add the number of users to the List of Departments page. Bonus points for efficiency.

* https://book.cakephp.org/4/en/orm/table-objects.html#lifecycle-callbacks